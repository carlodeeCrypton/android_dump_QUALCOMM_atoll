#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:77c6722edac5c25a3475366b8e04cb3b6dfae4dc; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:881e991206c2db8222009104cdb127d42ac16daa \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:77c6722edac5c25a3475366b8e04cb3b6dfae4dc && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
